package steps;

import org.hamcrest.Matchers;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.GoogleCalculator;
import org.jbehave.core.annotations.*;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.GoogleSearchPage;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class CalculatorSteps {

    public static String driverName = "chromedriver.exe";
    String workingDirectory = System.getProperty("user.dir");
    private String url = "https://www.google.com.ua/";
    public static WebDriver driver;

    @BeforeStory
    public void setup() {

        System.setProperty("webdriver.chrome.driver", workingDirectory + System.getProperty("file.separator") + driverName);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

        driver.get(url);
        GoogleSearchPage sp = new GoogleSearchPage(driver);
        sp.searchSuccess();
    }

    @Given("I am on Google calculator Page")
    public void getCurrentURL() {
        GoogleCalculator calc = new GoogleCalculator(driver);

        Assert.assertEquals(driver.getCurrentUrl(), calc.calculatorUrl());
    }

    @When("I pass <operand1> <mathOperator> <operand2> and click equals button")
    @Alias("I pass $operand1 $mathOperator $operand2 and click equals button")
    public void mathOperation(@Named("operand1") double operand1, @Named("operand2") double operand2,
                              @Named("mathOperator") String mathOperator) {
        GoogleCalculator calc = new GoogleCalculator(driver);
        calc.executeOperation(operand1, operand2, mathOperator);

    }

    @Then("<result> should be displayed")
    @Alias("$result should be displayed")
    public void verifyResult(@Named("result") String amount) {
        GoogleCalculator calc = new GoogleCalculator(driver);

        Assert.assertThat(calc.inputValue(), Matchers.equalTo(amount));
        ;
    }
}
