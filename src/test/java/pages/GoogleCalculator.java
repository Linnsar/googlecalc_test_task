package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleCalculator {


    private WebDriver driver;

    @FindBy(id = "cwtltblr")
    private WebElement calculator;

    @FindBy(xpath = "//div[@class='cwtlotc']/span")
    private WebElement inputInt;

    @FindBy(xpath = "//div[@class='cwbtpl cwdgb-tpl cwbbts']/descendant::span[text()='+']")
    private WebElement plus;

    @FindBy(xpath = "//*[@id=\"cwbt36\"]/div/span")
    private WebElement minus;

    @FindBy(xpath = "//*[@id=\"cwbt26\"]/div/span")
    private WebElement multiply;

    @FindBy(xpath = "//*[@id=\"cwbt16\"]/div/span")
    private WebElement divide;

    @FindBy(xpath = "//div[@class='cwbtpl cwbb-tpl cwbbts']/descendant::span[text()='=']")
    private WebElement equal;

    @FindBy(className = "cwtlotc")
    private WebElement inputLine;

    @FindBy(id = "cwclrbtnAC")
    private WebElement clearButton;


    public GoogleCalculator(WebDriver driver) {

        if (!driver.findElement(By.id("cwtltblr")).isDisplayed()) {
            throw new IllegalStateException(
                    "This is not the page you are expected"
            );
        }

        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    private void clearInput() {
        Actions actions = new Actions(driver);
        actions.moveToElement(inputInt);
        actions.click();
        int currentInput = Integer.parseInt(inputInt.getText());
        if (currentInput > 0) {

            actions.sendKeys("0");
        }
    }

    public void newInput() {
        clearInput();
    }

    private String currentPage() {

        String url = driver.getCurrentUrl();

        return url;
    }

    public String calculatorUrl() {

        String url = currentPage();

        return url;
    }

    private String mathOperation(Double operand1, Double operand2, String mathOperator) {
        Actions actions = new Actions(driver);
        String result;

        actions.moveToElement(inputInt);
        actions.click();
        actions.sendKeys(clearButton);
        actions.sendKeys(operand1.toString());

        if (mathOperator.equals("-")) {
            actions.click(minus);
        } else if (mathOperator.equals("+")) {
            actions.click(plus);
        } else if (mathOperator.equals("*")) {
            actions.click(multiply);
        } else if (mathOperator.equals("/")) {
            actions.click(divide);
        }

        actions.sendKeys(operand2.toString());
        actions.click(equal);
        actions.build().perform();

        result = inputInt.getText();

        return result;
    }


    public void executeOperation(Double operand1, Double operand2, String mathOperator) {

        mathOperation(operand1, operand2, mathOperator);
    }

    public String inputValue() {
        String inputValue = inputInt.getText();
        return inputValue;
    }

}
