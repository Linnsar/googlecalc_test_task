package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Mariia_Koltsova on 10/18/2017.
 */
public class GoogleSearchPage {

    private WebDriver driver;
    private String searchUrl = "https://www.google.com.ua";

    @FindBy(id = "lst-ib")
    private WebElement searchField;

    @FindBy(name= "btnK")
    private WebElement buttonSearch;



    public GoogleSearchPage(WebDriver driver) {

        if (!driver.getCurrentUrl().contains(searchUrl)) {
            throw new IllegalStateException(
                    "This is not the page you are expected"
            );
        }

        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    private void searchText(String searchTerm) {

        searchField.sendKeys(searchTerm);
        buttonSearch.submit();

    }

    public GoogleCalculator searchSuccess() {
        searchText("calc");
        return new GoogleCalculator(driver);
    }


}
