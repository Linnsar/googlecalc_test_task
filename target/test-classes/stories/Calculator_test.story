Meta:

Narrative: I should be able to Navigate to Google Calculator page and execute basic arithmetical
operations, such as Addition, Subtraction, Multiplication, Division, Division by 0.

Scenario: I should see the result of Addition of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|22|+|8|30|
|450|+|55.6|505.6|
|24.3|+|11.5|35.8|
|67.3|+|12.7|80|
|-45|+|50|5|


Scenario: I should see the result of Substraction of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|45|-|25|20|
|45.5|-|25.3|20.2|
|45.5|-|10|35.5|
|13|-|7.2|5.8|
|-20|-|13|-33|

Scenario: I should see the result of Multiplication of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|6|*|7|42|
|6.5|*|7.2|46.8|
|13.5|*|8|108|
|11|*|13.7|150.7|
|-45|*|34|-1530|


Scenario: I should see the result of Division of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|60|/|5|12|
|38760|/|456|85|
|247.5|/|75|3.3|
|126|/|8.4|15|
|38.475|/|8.55|4.5|

Scenario: I should see the result of Math Operation of dividing by zero

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|68|/|0|Infinity|
|-56|/|0|Infinity|

Scenario: I should see the result of Math Operation of Zero divided by zero

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|0|/|0|Error|



